/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.formatter.output;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

// TODO: Auto-generated Javadoc
/**
 * <p>Title: PathManager.java</p>
 * The Class PathManager.
 * <p>Description: </p>
 *
 * @author Phoenics Chow
 * @version 1.0
 * @date 2014-6-3
 */
public class PathManager {
		  
  		/** The series timestamp. */
  		private long seriesTimestamp;
		  
  		/** The base directory. */
  		private File baseDirectory;
		  
  		/** The file index. */
  		private AtomicInteger fileIndex;

		  /** The current file. */
  		private File currentFile;

		  /**
  		 * Instantiates a new path manager.
  		 */
  		public PathManager() {
		    seriesTimestamp = System.currentTimeMillis();
		    fileIndex = new AtomicInteger();
		  }

		  /**
  		 * Next file.
  		 *
  		 * @return the file
  		 */
  		public File nextFile() {
		    currentFile = new File(baseDirectory, seriesTimestamp + "-"
		        + fileIndex.incrementAndGet());

		    return currentFile;
		  }

		  /**
  		 * Gets the current file.
  		 *
  		 * @return the current file
  		 */
  		public File getCurrentFile() {
		    if (currentFile == null) {
		      return nextFile();
		    }

		    return currentFile;
		  }

		  /**
  		 * Rotate.
  		 */
  		public void rotate() {
		    currentFile = null;
		  }

		  /**
  		 * Gets the base directory.
  		 *
  		 * @return the base directory
  		 */
  		public File getBaseDirectory() {
		    return baseDirectory;
		  }

		  /**
  		 * Sets the base directory.
  		 *
  		 * @param baseDirectory the new base directory
  		 */
  		public void setBaseDirectory(File baseDirectory) {
		    this.baseDirectory = baseDirectory;
		  }

		  /**
  		 * Gets the series timestamp.
  		 *
  		 * @return the series timestamp
  		 */
  		public long getSeriesTimestamp() {
		    return seriesTimestamp;
		  }

		  /**
  		 * Gets the file index.
  		 *
  		 * @return the file index
  		 */
  		public AtomicInteger getFileIndex() {
		    return fileIndex;
		  }
}
