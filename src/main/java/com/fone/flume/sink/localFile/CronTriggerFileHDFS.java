/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.sink.localFile;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * <p>Title: CronTriggerFileHDFS.java</p>
 * The Class CronTriggerFileHDFS.
 * <p>Description: </p>
 *
 * @author Phoenics Chow
 * @version 1.0
 * @date 2014-6-3
 */
public class CronTriggerFileHDFS {
	
	/** The log. */
	static Logger LOG = LoggerFactory.getLogger(CronTriggerFileHDFS.class);
	
	/** The scheduler factory. */
	SchedulerFactory schedulerFactory = new StdSchedulerFactory();
	
	/** The scheduler. */
	Scheduler scheduler =null;
	
	/**
	 * Run.
	 *
	 * @param filePath the file path
	 * @param hdfsPath the hdfs path
	 * @param cronExpression the cron expression
	 * @param isKeep the is keep
	 * @throws Exception the exception
	 */
	public void run(String filePath, String hdfsPath, String cronExpression, boolean isKeep) throws Exception {
		if(scheduler ==null){
			scheduler = schedulerFactory.getScheduler();
		}
		 schedulerFactory = new StdSchedulerFactory();
		//Scheduler scheduler = schedulerFactory.getScheduler();
		JobDetail job = JobBuilder.newJob(File2HDFSJob.class).withIdentity("job1", "group1").build();
		JobDataMap jobDataMap = job.getJobDataMap();
		jobDataMap.put("filePath", filePath);
		jobDataMap.put("hdfsPath", hdfsPath);
		jobDataMap.put("isKeep", isKeep);
		LOG.info("hdfs path:{}, dot delete source:{} .",hdfsPath,isKeep);
		CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1")
				.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
				.build();
		scheduler.scheduleJob(job, cronTrigger);
		scheduler.start();
		//scheduler.shutdown(true);
	}
	
	/**
	 * Shutdown.
	 *
	 * @throws Exception the exception
	 */
	public void shutdown()throws Exception{
		if(scheduler !=null){
			scheduler.shutdown(true);
		}
	}

}
