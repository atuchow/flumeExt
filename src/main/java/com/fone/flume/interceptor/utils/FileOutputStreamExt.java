/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.interceptor.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

// TODO: Auto-generated Javadoc
/**
 * <p>Title: FileOutputStreamExt.java</p>
 * The Class FileOutputStreamExt.
 * <p>Description: </p>
 *
 * @author Phoenics Chow
 * @version 1.0
 * @date 2014-6-3
 */
public class FileOutputStreamExt extends FileOutputStream {
	
	/** The file. */
	final  private File file ;
	
	/** The is close. */
	private boolean isClose=false;
	private int bufSize=1024*1024; 
	private ByteBuffer wBuffer=null;
	/**
	 * Instantiates a new file output stream ext.
	 *
	 * @param file the file
	 * @throws FileNotFoundException the file not found exception
	 */
	public FileOutputStreamExt(File file) throws FileNotFoundException {
		this(file,true);  
	}
	
	/**
	 * Instantiates a new file output stream ext.
	 *
	 * @param file the file
	 * @param append the append
	 * @throws FileNotFoundException the file not found exception
	 */
	public FileOutputStreamExt(File file, boolean append) throws FileNotFoundException {
		super(file,append);
		this.file=file;
		wBuffer=ByteBuffer.allocate(bufSize);
		wBuffer.clear();
	}
	
	/**
	 * Instantiates a new file output stream ext.
	 *
	 * @param name the name
	 * @throws FileNotFoundException the file not found exception
	 */
	public FileOutputStreamExt(String name)  throws FileNotFoundException {
		this(new File(name));
	}
	
	/**
	 * Instantiates a new file output stream ext.
	 *
	 * @param name the name
	 * @param append the append
	 * @throws FileNotFoundException the file not found exception
	 */
	public FileOutputStreamExt(String name, boolean append)  throws FileNotFoundException {
		this(new File(name),append);
	}
	
	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public File getFile() {
		return file;
	}
	
	/* (non-Javadoc)
	 * @see java.io.FileOutputStream#close()
	 */
	@Override
	public void close() throws IOException{
		flush();
		this.getChannel().close();
		super.close();
		setClose(true);
	}
	
	/**
	 * Checks if is close.
	 *
	 * @return true, if is close
	 */
	public boolean isClose() {
		return isClose;
	}
	@Override
	public void write(byte[] b) throws IOException{
		if(wBuffer.remaining()<b.length){
			flush();
		}
		wBuffer.put(b);
	}
	@Override
	public void flush() throws IOException{
		if(wBuffer.position()==0 && wBuffer.limit()==wBuffer.capacity()){
			return;
		}
		wBuffer.flip();
		this.getChannel().write(wBuffer);
		wBuffer.clear();
	}
	
	/**
	 * Sets the close.
	 *
	 * @param isClose the new close
	 */
	private void setClose(boolean isClose) {
		this.isClose = isClose;
	}
	


}
