/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.interceptor.utils;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
// TODO: Auto-generated Javadoc

/**
 * <p>Title: FileWatchdog.java</p>
 * The Class FileWatchdog.
 * <p>Description: </p>
 *
 * @author Phoenics Chow
 * @version 1.0
 * @date 2014-4-24
 */
public abstract class FileWatchdog extends Thread {

  /** The log. */
  static Logger log = LoggerFactory.getLogger(FileWatchdog.class.getName());

  /** The Constant DEFAULT_DELAY. */
  static final public long DEFAULT_DELAY = 6000;

  /** The filename. */
  protected String filename;
  
  /** The delay. */
  protected long delay = DEFAULT_DELAY; 
  
  /** The file. */
  File file;
  
  /** The last modified. */
  long lastModified = 0; 
  
  /** The warned already. */
  boolean warnedAlready = false;
  
  /** The interrupted. */
  boolean interrupted = false;

  /**
   * Instantiates a new file watchdog.
   */
  protected FileWatchdog() {
    super();
  }
  
  /**
   * Instantiates a new file watchdog.
   *
   * @param filename the filename
   */
  protected FileWatchdog(String filename) {
    initialize(filename);
  }
  
  /**
   * Initialize.
   *
   * @param filename the filename
   */
  protected void initialize(String filename) {
    this.filename = filename;
    file = new File(filename);
    setDaemon(true);
    try {
      log.debug("Doing initial check");
      checkAndConfigure();
    } catch (Exception e) {
      log.warn("Error checking file", e);
    }
  }
  
  /**
   * Sets the delay.
   *
   * @param delay the new delay
   */
  public void setDelay(long delay) {
    this.delay = delay;
    if (delay < 0)
      this.interrupted = true;
  }

  /**
   * Do on change.
   */
  abstract protected void doOnChange();

  /**
   * Check and configure.
   */
  protected void checkAndConfigure() {
    boolean fileExists;
    try {
      fileExists = file.exists();
    } catch (SecurityException e) {
      log.warn("Was not allowed to check existence of file: "+ filename);
      interrupted = true;
      return;
    }
    if (fileExists) {
      long l = file.lastModified();
      log.debug(filename + " last modified " + l + "; previously " + lastModified);
      if (l > lastModified) {
        log.debug("loading file " + filename);
        lastModified = l;
        doOnChange();
        warnedAlready = false;
      }
    } else {
      if (!warnedAlready) {
        log.debug("["+filename+"] does not exist.");
        warnedAlready = true;
      }
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Thread#run()
   */
  @SuppressWarnings("static-access")
public void run() {
    while (!interrupted) {
      try {
        Thread.currentThread().sleep(delay);
      } catch (InterruptedException e) {
        // no interruption expected
      }
      try {
        log.debug("Checking and configuring");
        checkAndConfigure();
      } catch (Exception e) {
        log.warn("Error checking file", e);
      }
    }
  }

}