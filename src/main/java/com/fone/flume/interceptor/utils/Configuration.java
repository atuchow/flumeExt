/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.interceptor.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;






// TODO: Auto-generated Javadoc
/**
 * <p>Title: Configuration.java</p>
 * The Class Configuration.
 * <p>Description: </p>
 *
 * @author Phoenics Chow
 * @version 1.0
 * @date 2014-4-24
 */
public class Configuration {
	
	/** The object set. */
	private static Map<String,Configuration> objectSet=new HashMap<>();
	
	/** The propertie. */
	private Properties propertie;
    
    /** The input file. */
    private FileInputStream inputFile;
    
    /**
     * Instantiates a new configuration.
     *
     * @param filename the filename
     */
    private Configuration(String filename){
    	//wc=new ConfigWatchdog(filename);
    }
    
    /**
     * Gets the single instance of Configuration.
     *
     * @param filename the filename
     * @return single instance of Configuration
     */
    public static synchronized Configuration getInstance(String filename) {  
    	if(filename==null || filename.trim().equals("")){
    		return null;
    	}
    	File f=new File(filename);
    	if(!f.exists()){
    		return null;
    	}
    	Configuration instance=objectSet.get(filename);
		if(instance==null){
			instance = new Configuration(filename); 
			instance.load(filename);
			new ConfigWatchdog(filename,instance).start();
			if(instance!=null){
				objectSet.put(filename, instance);
			}
		}
		return instance;
    	
    }
    
    /**
     * Load.
     *
     * @param filePath the file path
     */
    public void load(String filePath){
    	if(propertie!=null){
    		propertie.clear();
    	}
    	propertie = new Properties();
        try {
            inputFile = new FileInputStream(filePath);
            propertie.load(inputFile);
            //propertie.list(System.out);
            inputFile.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Gets the value.
     *
     * @param key the key
     * @return the value
     */
    public String getValue(String key){
        if(propertie.containsKey(key)){
            String value = propertie.getProperty(key);//得到某一属性的值
            return value;
        } else 
            return key.replaceAll("\\.", "_").toUpperCase();
    }
    
    /**
     * Clear.
     */
    public void clear(){
        propertie.clear();
    }
    
    /**
     * Sets the value.
     *
     * @param key the key
     * @param value the value
     */
    public void setValue(String key, String value){
        propertie.setProperty(key, value);
    }

}
