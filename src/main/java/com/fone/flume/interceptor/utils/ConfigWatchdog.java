/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.interceptor.utils;

// TODO: Auto-generated Javadoc
/**
 * <p>Title: ConfigWatchdog.java</p>
 * The Class ConfigWatchdog.
 * <p>Description: </p>
 *
 * @author Phoenics Chow
 * @version 1.0
 * @date 2014-4-24
 */
public class ConfigWatchdog  extends FileWatchdog {

	/** The configuration. */
	private Configuration configuration;//=Configuration.getInstance(filename);
	
	/**
	 * Instantiates a new config watchdog.
	 *
	 * @param filename the filename
	 * @param configuration the configuration
	 */
	public ConfigWatchdog(String filename,Configuration configuration) {  
        super(filename);  
        this.configuration=configuration;
	}
	
	/* (non-Javadoc)
	 * @see com.fone.flume.interceptor.utils.FileWatchdog#doOnChange()
	 */
	@Override
	protected void doOnChange() {
		if(configuration!=null)
		configuration.load(filename);
	}
}
