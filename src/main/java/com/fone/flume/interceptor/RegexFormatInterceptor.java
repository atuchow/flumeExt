/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.interceptor;

import static com.fone.flume.interceptor.RegexFormatInterceptor.Constants.DEFAULT_EXCLUDE_EVENTS;
import static com.fone.flume.interceptor.RegexFormatInterceptor.Constants.DEFAULT_REGEX;
import static com.fone.flume.interceptor.RegexFormatInterceptor.Constants.EXCLUDE_EVENTS;
import static com.fone.flume.interceptor.RegexFormatInterceptor.Constants.REGEX;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;

// TODO: Auto-generated Javadoc
/**
 * The Class RegexFormatInterceptor.
 *
 * @author Phoenics Chow
 */
public class RegexFormatInterceptor implements Interceptor {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(RegexFormatInterceptor.class);

	/** The regex. */
	private final Pattern regex;
	
	/** The exclude events. */
	private final boolean excludeEvents;

	/**
	 * Only {@link RegexFilteringInterceptor.Builder} can build me
	 *
	 * @param regex the regex
	 * @param excludeEvents the exclude events
	 */
	private RegexFormatInterceptor(Pattern regex, boolean excludeEvents) {
		this.regex = regex;
		this.excludeEvents = excludeEvents;
	}

	/* (non-Javadoc)
	 * @see org.apache.flume.interceptor.Interceptor#initialize()
	 */
	@Override
	public void initialize() {
		// no-op
	}

	/* (non-Javadoc)
	 * @see org.apache.flume.interceptor.Interceptor#intercept(org.apache.flume.Event)
	 */
	@Override
	/**
	 * Returns the event if it passes the regular expression filter and null
	 * otherwise.
	 */
	public Event intercept(Event event) {
		// We've already ensured here that at most one of includeRegex and
		// excludeRegex are defined.
		if (event == null) {
			return null;
		}
		try {
			String e_b = new String(event.getBody(), Charsets.UTF_8);
			Matcher matcher = regex.matcher(e_b);
			boolean r_f = matcher.find();
			if (!r_f) {
				return event;
			}
			String eventb = e_b;
			if (excludeEvents) {
				eventb = matcher.replaceFirst("").trim();
			} else {
				eventb = matcher.group().trim();
			}
			if (eventb.equals("")) {
				return event;
			}
			event.setBody(eventb.getBytes(Charsets.UTF_8));
			return event;
		} catch (Exception e) {
			return event;
		}

	}

	/**
	 * Returns the set of events which pass filters, according to
	 * {@link #intercept(Event)}.
	 *
	 * @param events the events
	 * @return the list
	 */
	@Override
	public List<Event> intercept(List<Event> events) {
		List<Event> out = Lists.newArrayList();
		for (Event event : events) {
			Event outEvent = intercept(event);
			if (outEvent != null) {
				out.add(outEvent);
			}
		}
		return out;
	}

	/* (non-Javadoc)
	 * @see org.apache.flume.interceptor.Interceptor#close()
	 */
	@Override
	public void close() {
		// no-op
	}

	/**
	 * Builder which builds new instance of the StaticInterceptor.
	 *
	 * @author Phoenics Chow
	 */
	public static class Builder implements Interceptor.Builder {

		/** The regex. */
		private Pattern regex;
		
		/** The exclude events. */
		private boolean excludeEvents;

		/* (non-Javadoc)
		 * @see org.apache.flume.conf.Configurable#configure(org.apache.flume.Context)
		 */
		@Override
		public void configure(Context context) {
			// excludeEvents=false;
			String regexString = context.getString(REGEX, DEFAULT_REGEX);
			regex = Pattern.compile(regexString);
			excludeEvents = context.getBoolean(EXCLUDE_EVENTS, DEFAULT_EXCLUDE_EVENTS);
		}

		/* (non-Javadoc)
		 * @see org.apache.flume.interceptor.Interceptor.Builder#build()
		 */
		@Override
		public Interceptor build() {
			logger.info(String.format("Creating RegexFormatInterceptor: regex=%s,replace=%s", regex, excludeEvents));
			return new RegexFormatInterceptor(regex, excludeEvents);
		}
	}

	/**
	 * The Class Constants.
	 *
	 * @author Phoenics Chow
	 */
	public static class Constants {

		/** The Constant REGEX. */
		public static final String REGEX = "regex";
		
		/** The Constant DEFAULT_REGEX. */
		public static final String DEFAULT_REGEX = ".*";

		/** The Constant EXCLUDE_EVENTS. */
		public static final String EXCLUDE_EVENTS = "replace";
		
		/** The Constant DEFAULT_EXCLUDE_EVENTS. */
		public static final boolean DEFAULT_EXCLUDE_EVENTS = false;
	}

}