/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.flume.Event;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.FlumeException;
import org.apache.flume.api.RpcClient;
import org.apache.flume.api.RpcClientConfigurationConstants;
import org.apache.flume.api.RpcClientFactory;
import org.apache.flume.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * Title: ReadAvroLocalFile.java
 * </p>
 * The Class ReadAvroLocalFile.
 * <p>
 * Description:
 * </p>
 *
 * @author Phoenics Chow
 * @version 1.0
 * @date 2014-5-6
 */
public class ReadAvroLocalFile {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ReadAvroLocalFile.class);

	/** The hostname. */
	private String hostname;

	/** The local path. */
	private String localPath;

	/** The segment. */
	private long segment = 0;

	/** The port. */
	private String port;

	/** The unsafe mode. */
	private boolean unsafeMode = false;

	/** The timeout. */
	private long timeout = RpcClientConfigurationConstants.DEFAULT_REQUEST_TIMEOUT_MILLIS;

	/** The rpc client. */
	private RpcClient rpcClient = null;
	
	/** The calendar. */
	private Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+08:00"));
	// private EventSerializer serializer;

	/**
	 * <p>
	 * Title: ReadAvroLocalFile.java
	 * </p>
	 * The Enum AvroSchemaType.
	 * <p>
	 * Description:
	 * </p>
	 *
	 * @author Phoenics Chow
	 * @version 1.0
	 * @date 2014-5-6
	 */
	public static enum AvroSchemaType {

		/** The hash. */
		HASH,

		/** The literal. */
		LITERAL;
	}

	/** The Constant CONFIG_SCHEMA_TYPE_KEY. */
	public static final String CONFIG_SCHEMA_TYPE_KEY = "schemaType";

	/** The Constant AVRO_SCHEMA_HEADER_HASH. */
	public static final String AVRO_SCHEMA_HEADER_HASH = "flume.avro.schema.hash";

	/** The Constant AVRO_SCHEMA_HEADER_LITERAL. */
	public static final String AVRO_SCHEMA_HEADER_LITERAL = "flume.avro.schema.literal";

	/**
	 * Instantiates a new read avro local file.
	 *
	 * @param hostname
	 *            the hostname
	 * @param port
	 *            the port
	 * @param localPath
	 *            the local path
	 * @param segment
	 *            the segment
	 */
	public ReadAvroLocalFile(String hostname, String port, String localPath, String segment) {
		this.hostname = hostname;
		this.port = port;
		this.localPath = localPath;
		this.segment = string2Long(segment);
		if (hostname != null) {
			this.activateOptions();
		}
	}
	
	/**
	 * Instantiates a new read avro local file.
	 *
	 * @param localPath the local path
	 * @param segment the segment
	 */
	public ReadAvroLocalFile(String localPath, String segment) {
		this(null,"0",localPath,segment);
	}
	
	/**
	 * String2 long.
	 *
	 * @param s the s
	 * @return the long
	 */
	private long string2Long(String s){
		long ss=0;
		try{
			ss=Long.parseLong(s);
		}catch(NumberFormatException e){
			ss=0;
		}
		return ss;
	}

	/**
	 * Append.
	 *
	 * @param event
	 *            the event
	 */
	public void append(Event event) {
		if (rpcClient != null) {
			try {
				rpcClient.append(event);
				
			} catch (EventDeliveryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Appends.
	 *
	 * @param events
	 *            the events
	 */
	public void appends(List<Event> events) {
		if (rpcClient != null) {
			try {
				rpcClient.appendBatch(events);
			} catch (EventDeliveryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Send to flume.
	 */
	public void sendToFlume() {
		List<File> files = getFileList();
		for (File f : files) {
			this.appends(fileToEvents(f));
		}
	}

	/**
	 * Testthis.
	 *
	 * @param fileStr
	 *            the file str
	 */
	@SuppressWarnings("rawtypes")
	public void testthis(String fileStr) {
		File f = new File(fileStr);
		List<Event> t = fileToEvents(f);
		for (Event e : t) {
			System.out.println("+++++++++++++++++++++++");
			Map<String, String> header = e.getHeaders();
			System.out.println("==========Headers===========");
			for (Map.Entry me : header.entrySet()) {
				System.out.println(me.getKey().toString() + " = " + me.getValue().toString());
			}
			System.out.println("==========Body===========");
			try {
				System.out.println(new String(e.getBody(), "utf-8"));
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}

		}
	}
	

	/**
	 * File to events.
	 *
	 * @param file
	 *            the file
	 * @return the list
	 */
	@SuppressWarnings({  "unchecked" })
	private List<Event> fileToEvents(File file) {
		List<Event> r = Lists.newArrayList();
		DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>();
		DataFileReader<GenericRecord> dataFileReader =null;
		try {
			 dataFileReader = new DataFileReader<GenericRecord>(file, reader);
			for (GenericRecord record : dataFileReader) {
				ByteBuffer body = (ByteBuffer) record.get("body");
				Event event = EventBuilder.withBody(body.array());
				event.getHeaders().putAll((Map<String, String>) record.get("headers"));
				r.add(event);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return r;
		} finally {
			if(dataFileReader!=null)
				try {
					dataFileReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return r;
	}
	


	/**
	 * Save one file.
	 *
	 * @param file the file
	 * @return true, if successful
	 */
	private boolean saveOneFile(File file){
		DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>();
		Map<String ,FileWriter> filesmap=new HashMap<>();
		DataFileReader<GenericRecord> dataFileReader=null;
		try {
			dataFileReader = new DataFileReader<GenericRecord>(file, reader);
			//List<Event> r = Lists.newArrayList();
			for (GenericRecord record : dataFileReader) {
				Map<String, String> headers=new HashMap<>();
				ByteBuffer body = (ByteBuffer) record.get("body");
				String bodyStr=new String(body.array());
				String record_str=record.get("headers").toString();
				if(record_str!=null){
					record_str=record_str.trim();
					int rleng=record_str.length();
					record_str=record_str.substring(1,rleng-1);
					//System.out.println("record_str="+record_str);
					String [] record_s=record_str.split(",");
					for(String kv: record_s){
						String [] kvss=kv.split("=");
						headers.put(kvss[0].trim(), kvss[1].trim());
					}
				}
				//正式版删除这个部分
				
		/*		if(bodyStr.indexOf("LOG_LOGIN")>=0){
					headers.put("flume.client.log4j.logger.name", "log_login");
				}
				if(bodyStr.indexOf("LOG_USER")>=0){
					headers.put("flume.client.log4j.logger.name", "log_user");
				}
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+08:00"));
				String time_name = headers.get("flume.client.log4j.timestamp");

				if(time_name==null){
					time_name=file.lastModified()+"";
				}
				if (time_name != null && !time_name.trim().equals("") && time_name.replaceAll("\\d", "").trim().equals("")) {
					long v = Long.parseLong(time_name.trim());
					calendar.setTimeInMillis(v);
					int year = calendar.get(Calendar.YEAR);
					int month = calendar.get(Calendar.MONTH) + 1; // 获取月份，0表示1月份
					int day = calendar.get(Calendar.DAY_OF_MONTH); // 获取当前天数
					int hour = calendar.get(Calendar.HOUR_OF_DAY); // 获取当前小时
					int min = calendar.get(Calendar.MINUTE); // 获取当前分钟
					int ss = calendar.get(Calendar.SECOND); // 获取当前秒
					headers.put("fy", ""+year);
					if(month<10){
						headers.put("fm", "0"+month);
					}else{
						headers.put("fm", ""+month);
					}
					if(day<10){
						headers.put("fd", "0"+day);
					}else{
						headers.put("fd", ""+day);
					}
					if(hour<10){
						headers.put("fh", "0"+hour);
					}else{
						headers.put("fh", ""+hour);
					}
					if(min<10){
						headers.put("fmin", "0"+min);
					}else{
						headers.put("fmin", ""+min);
					}
					if(ss<10){
						headers.put("fs", "0"+ss);
					}else{
						headers.put("fs", ""+ss);
					}
				}else{
					LOG.warn("no  timestamp!");
				}*/
			//正式版删除上面的部分
				//flume.client.log4j.timestamp=1400691816378
				String fh=headers.get("fh");
				String fmin=headers.get("fmin");
				String timestamp=headers.get("flume.client.log4j.timestamp");
				if(timestamp==null){
					timestamp="";
				}
				if(fh==null || fh.trim().equals("")){
					if(! timestamp.equals("")){
						calendar.setTimeInMillis(Long.parseLong(timestamp.trim()));
						int hour = calendar.get(Calendar.HOUR_OF_DAY); // 获取当前小时
						int min = calendar.get(Calendar.MINUTE); // 获取当前分钟
						if(hour<10){
							headers.put("fh", "0"+hour);
						}else{
							headers.put("fh", ""+hour);
						}
						if(min<10){
							headers.put("fmin", "0"+min);
						}else{
							headers.put("fmin", ""+min);
						}
					}
				}
				
				String fm=headers.get("fm");
				String fy=headers.get("fy");
				String fd=headers.get("fd");
				 fh=headers.get("fh");
				 fmin=headers.get("fmin");
				 fmin=fmin.substring(0, 1)+"0";
				//String fs=headers.get("fs");
				String xi=fy+"-"+fm+"-"+fd+"-"+fh+"-"+fmin+".";
				String tablename=headers.get("tablename");
				//System.out.println("tablename===="+tablename);
				if(tablename==null||tablename.equals("")){
					tablename=headers.get("flume.client.log4j.logger.name");
				}else{
					tablename=tablename.toLowerCase();
				}
				
				String filename="generic/"+tablename+"/part="+fy+fm+fd;
				int  ll=file.getPath().length()-file.getName().length();
				filename=file.getPath().substring(0, ll)+"/"+filename;
				filename=filename.replaceAll("null", "_a");
				File path=new File(filename);
	
				if(!path.exists()){
					path.mkdirs();
				}
				//  /user/hive/warehouse/player.db/log_login/part=20140505/LOG_LOGIN.9.log.2014-05-05.1399282413811
				filename=filename+"/"+tablename+"."+xi+file.getName();
				filename=filename.replaceAll("//", "/");
				FileWriter fw= filesmap.get(filename);
				if( fw==null){
					//FileOutputStream fos=new FileOutputStream(new File(filename),true);
					File f=new File(filename);
					if(f.exists()){
						f.delete();
					}
					fw = new FileWriter(f,true);
					System.out.println("开始打开新文件:"+filename+" 写入信息");
					filesmap.put(filename, fw);
					//fos.write(body.array());
				}
				fw.append(bodyStr+"\n");
				fw.flush();
			}
			
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}  catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			
			Set<String> fileNames=filesmap.keySet();
			try {
				if(dataFileReader!=null){
					dataFileReader.close();
				}
			for(String fileName: fileNames){
				FileWriter fw= filesmap.get(fileName);
				if(fw!=null){
					fw.close();
					System.out.println("文件:"+fileName+" 关闭");
				}
			}
	
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
	}
	
	/**
	 * Save all file.
	 *
	 * @param del the del
	 */
	public void saveAllFile(boolean del){
		long se = getCurrentTime();
		File failoverpath = new File(localPath);
		if (!failoverpath.exists() || failoverpath.isFile()) {
			return ;
		}
		
		File[] fs = failoverpath.listFiles();
		for (File f : fs) {
			if (f.isFile()) {
				if (filterFile(f, se)) {
					System.out.println("处理文件:"+f.getName());
					if(saveOneFile(f)){
						if(del){
							f.delete();
						}
					}
					
				}
			}
		}
		
	}
	
	/**
	 * Send one file.
	 *
	 * @param file the file
	 * @return true, if successful
	 */
	@SuppressWarnings({ "unchecked" }) 
	private boolean sendOneFile(File file){
		DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>();
		if(rpcClient==null || !rpcClient.isActive()){
			this.activateOptions();
		}
		DataFileReader<GenericRecord> dataFileReader =null;
		try {
			 dataFileReader = new DataFileReader<GenericRecord>(file, reader);
			//List<Event> r = Lists.newArrayList();
			for (GenericRecord record : dataFileReader) {
				ByteBuffer body = (ByteBuffer) record.get("body");
				Event event = EventBuilder.withBody(body.array());
				event.getHeaders().putAll((Map<String, String>) record.get("headers"));
				this.append(event);
			}
			
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}  catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			if(dataFileReader!=null){
				try {
					dataFileReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(rpcClient!=null){
				rpcClient.close();
				rpcClient=null;
			}
		}
	}
	
	/**
	 * Send all file.
	 *
	 * @return the map
	 */
	public Map<File,Boolean> sendAllFile(){
		Map<File,Boolean> rt=new HashMap<>();
		long se = getCurrentTime();
		File failoverpath = new File(localPath);
		if (!failoverpath.exists() || failoverpath.isFile()) {
			return rt;
		}
		File[] fs = failoverpath.listFiles();
		for (File f : fs) {
			if (f.isFile()) {
				if (filterFile(f, se)) {
					boolean yes=false;
					if(sendOneFile(f)){
						yes=true;
					}
					rt.put(f, yes);
					//break;// ===================================
				}
			}
		}
		return rt;
	}

	/**
	 * Gets the file list.
	 *
	 * @return the file list
	 */
	private List<File> getFileList() {
		long se = getCurrentTime();
		List<File> files = new ArrayList<>();
		File failoverpath = new File(localPath);
		if (!failoverpath.exists()) {
			return files;
		}
		if (!failoverpath.isDirectory()) {
			return files;
		}
		File[] fs = failoverpath.listFiles();
		for (File f : fs) {
			if (f.isFile()) {
				if (filterFile(f, se)) {
					files.add(f);
				}
			}
		}
		return files;
	}

	/**
	 * Filter file.
	 *
	 * @param f
	 *            the f
	 * @param se
	 *            the se
	 * @return true, if successful
	 */
	private boolean filterFile(File f, long se) {

		if (f.lastModified() <= se - segment) {
			return true;
		}
		return false;
	}

	/**
	 * Gets the current time.
	 *
	 * @return the current time
	 */
	private long getCurrentTime() {
		return System.currentTimeMillis();
	}

	/**
	 * Activate options.
	 *
	 * @throws FlumeException
	 *             the flume exception
	 */
	public void activateOptions() throws FlumeException {
		Properties props = new Properties();
		props.setProperty(RpcClientConfigurationConstants.CONFIG_HOSTS, "h1");
		props.setProperty(RpcClientConfigurationConstants.CONFIG_HOSTS_PREFIX + "h1", hostname + ":" + port);
		props.setProperty(RpcClientConfigurationConstants.CONFIG_CONNECT_TIMEOUT, String.valueOf(timeout));
		props.setProperty(RpcClientConfigurationConstants.CONFIG_REQUEST_TIMEOUT, String.valueOf(timeout));
		try {
			//System.out.println("=========+++========");
			rpcClient = RpcClientFactory.getInstance(props);
			//System.out.println("=================00");
		} catch (FlumeException e) {
			String errormsg = "RPC client creation failed! " + e.getMessage();
			LOG.error(errormsg);
			if (unsafeMode) {
				return;
			}
			throw e;
		}
	}
	
	/**
	 * Write file by line.
	 *
	 * @param fcout the fcout
	 * @param wBuffer the w buffer
	 * @param line the line
	 */
	public static void writeFileByLine(FileChannel fcout, ByteBuffer wBuffer, String line){ 
		try { 
			fcout.write(ByteBuffer.wrap(line.getBytes()), fcout.size()); 

		} catch (IOException e) { 
			// TODO Auto-generated catch block 
			e.printStackTrace(); 
		} 
	} 

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		if(args.length<2){
	            System.out.println("参数传递不完成,请补足！");  
	            return;  
		}
		String path=args[0];
		String segment=args[1];
		String del="false";
		if(args.length>2){
			 del=args[2];
			 if(del==null){
				 del="false";
			 }
		}
		
		ReadAvroLocalFile readAvroLocalFile = new ReadAvroLocalFile(path,segment);
		boolean d=Boolean.parseBoolean(del);
		readAvroLocalFile.saveAllFile(d);
		/*if(args.length<4){
		            System.out.println("参数传递不完成,请补足！");  
		            return;  
		}
		String host=args[0];
		String port=args[1];
		String path=args[2];
		String segment=args[3];
		//String timeout=args[4];
		
		ReadAvroLocalFile readAvroLocalFile = new ReadAvroLocalFile(host,port,path,segment);
		Map<File,Boolean> filemap=readAvroLocalFile.sendAllFile();
		Set<File>files= filemap.keySet();
		for(File file: files){
			if(filemap.get(file)){
				file.delete();
			}
		}*/
		//r.testthis("d:/tt/1399274617966-2");
	}

}
