/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.channel.jeChannel;

import java.io.File;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.concurrent.GuardedBy;

import org.apache.flume.ChannelException;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.annotations.Disposable;
import org.apache.flume.channel.BasicChannelSemantics;
import org.apache.flume.channel.BasicTransactionSemantics;
import org.apache.flume.instrumentation.ChannelCounter;
import org.apache.flume.annotations.InterfaceAudience;
import org.apache.flume.annotations.InterfaceStability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.sleepycat.collections.CurrentTransaction;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.DatabaseExistsException;
import com.sleepycat.je.DatabaseNotFoundException;
import com.sleepycat.je.Durability;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockConflictException;
import com.sleepycat.je.TransactionConfig;

// TODO: Auto-generated Javadoc
/**
 * The Class JEChannel.
 * 
 * @author Phoenics Chow
 */
@InterfaceAudience.Private
@InterfaceStability.Stable
@Disposable
public class JEChannel extends BasicChannelSemantics {

	/** The Constant LOG. */

	private static final Logger LOG = LoggerFactory.getLogger(JEChannel.class);
	
	/** The Constant defaultCapacity. */
	private static final Integer defaultCapacity = 100;
	
	
	/** The channel counter. */
	private ChannelCounter channelCounter;
	
	/** The db dir. */
	private String dbDir;
	
	/** The db name. */
	private String dbName="flumeDB";

	
	/** The trans capacity. */
	private volatile Integer transCapacity;
	
	/**
	 * The Class JETransaction.
	 *
	 * @author Phoenics Chow
	 */
	private class JETransaction extends BasicTransactionSemantics {
		
	
		/** The db env. */
		//private  BdbEnvironment dbEnv; 
		

private CurrentTransaction  currentTransaction;
		/** The put list. */
		private LinkedBlockingDeque<FlumeEvent> putList;
		
		/** The take count. */
		private int takeCount=0;
		
		
		/**  Lock held by take, poll, etc. */
		private final ReentrantLock lock = new ReentrantLock();
		/**  Wait queue for waiting takes. */
		private final Condition notEmpty = lock.newCondition();
		//private final Condition notFull = lock.newCondition();
		/** The db env. */
		private BdbEnvironment dbEnv;
		/**
		 * Instantiates a new JE transaction.
		 *
		 * @param counter the counter
		 * @param dbEnv the db env
		 */
		private JETransaction(ChannelCounter counter,BdbEnvironment dbEnv) {
			putList = new LinkedBlockingDeque<>(transCapacity);
			takeCount=0;
			channelCounter = counter;
			this.dbEnv=dbEnv;
		}
		
		/* (non-Javadoc)
		 * @see org.apache.flume.channel.BasicTransactionSemantics#doBegin()
		 */
		@Override
		protected void doBegin() throws InterruptedException {
			if(currentTransaction==null){
				currentTransaction=CurrentTransaction.getInstance(dbEnv);
			}
			currentTransaction.beginTransaction(TransactionConfig.DEFAULT);
			//BdbPersistentQueue
		}
		
		/* (non-Javadoc)
		 * @see org.apache.flume.channel.BasicTransactionSemantics#doClose()
		 */
		@Override
		protected void doClose() {
			currentTransaction=null;
		}
		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.flume.channel.BasicTransactionSemantics#doCommit()
		 */
		@Override
		protected void doCommit() throws InterruptedException {
			int puts = putList.size();
			final ReentrantLock putLock=this.lock;
			putLock.lockInterruptibly();
			 try{
				 if(puts>0){
					 while(!putList.isEmpty()) {
				            if(!queue.offer(putList.removeFirst())) {
				              throw new RuntimeException("Queue add failed, this shouldn't be able to happen");
				            }
				          }
					 currentTransaction.commitTransaction();
					 if(queue.size()>10){
							notEmpty.signal();
						}
				 }
				 
					
			 }catch (LockConflictException de) {
		    	  de.printStackTrace();
		      }catch (DatabaseException e) {
		    	  e.printStackTrace();
		      } finally {
		    	  putLock.unlock();
				}
			 putList.clear();
			 if (puts > 0) {
			        channelCounter.addToEventPutSuccessCount(puts);
			        //System.out.println("==============puts===-----"+puts);
			      }
			      if (takeCount > 0) {
			        channelCounter.addToEventTakeSuccessCount(takeCount);
			        takeCount=0;
			      }
			     // System.out.println("==========================="+queue.size()+"-----");
		      channelCounter.setChannelSize(queue.size());
		}
		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.flume.channel.BasicTransactionSemantics#doRollback()
		 */
		@Override
		protected void doRollback() throws InterruptedException {
			currentTransaction.abortTransaction();
			  channelCounter.setChannelSize(queue.size());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.apache.flume.channel.BasicTransactionSemantics#doPut(org.apache
		 * .flume.Event)
		 */
		@Override
		protected void doPut(Event event) throws InterruptedException {
			if(event==null){
				return;
			}
			channelCounter.incrementEventPutAttemptCount();
			if (!putList.offer(new FlumeEvent(event.getHeaders(),event.getBody()))) {
		        throw new ChannelException(
		          "Put queue for JETransaction of capacity " +
		            putList.size() + " full, consider committing more frequently, " +
		            "increasing capacity or increasing thread count");
		      }
		}
		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.flume.channel.BasicTransactionSemantics#doTake()
		 */
		@Override
		protected Event doTake() throws InterruptedException {
			System.out.println("============doTake==1111========");
			channelCounter.incrementEventTakeAttemptCount();
			Event event=null;
			final ReentrantLock takeLock = this.lock;
			takeLock.lockInterruptibly();
			try {
				System.out.println("============doTake==222222========");
				while (queue.size()== 0) {
					notEmpty.await();
				}
				event=queue.poll();
				takeCount++;
				Preconditions.checkNotNull(event, "Queue.poll returned NULL despite semaphore " + "signalling existence of entry");
				System.out.println("============doTake===event======="+ new String(event.getBody()));
				System.console().readLine();
				return event;
			} finally {
				takeLock.unlock();
			}
		}
}
		
	/**
	 * The Class JEConf.
	 *
	 * @author Phoenics Chow
	 */
	private class JEConf{
		
		/** The db env. */
		public  BdbEnvironment dbEnv=null; 
		
		/** The db. */
		public Database db=null;
	}
	
	/**
	 * Instantiates a new JE channel.
	 */
	public JEChannel(){
		super();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.flume.channel.BasicChannelSemantics#createTransaction()
	 */
	@Override
	protected BasicTransactionSemantics createTransaction() {
		if(jf==null){
			
		}
		if(jf.dbEnv==null){
			return null;
		}
		return new JETransaction(channelCounter,jf.dbEnv);
	}
	
	/** The jf. */
	private JEConf jf=null;
	
	//private Object queueLock = new Object();

	  /** The queue. */
	@GuardedBy(value = "queueLock")
	private JEPersistentQueue<FlumeEvent> queue;
	/**
	 * Creates the database.
	 *
	 * @param dbDir the db dir
	 * @param dbName the db name
	 * @return the JE conf
	 */
	private JEConf createDatabase(String dbDir, String dbName)  {
		File envFile = null;
		EnvironmentConfig envConfig = null;
		DatabaseConfig dbConfig = null;
		JEConf jeconf=new JEConf();
		try {
			// 数据库位置
			envFile = new File(dbDir);
			if(!envFile.exists()){
				envFile.mkdirs();
			}

			// 数据库环境配置
			envConfig = new EnvironmentConfig();
			envConfig.setAllowCreate(true);
			envConfig.setTransactional(true);
			envConfig.setDurability(Durability.COMMIT_WRITE_NO_SYNC);  

			// 数据库配置
			dbConfig = new DatabaseConfig();
			dbConfig.setAllowCreate(true);
			dbConfig.setTransactional(true);
			//dbConfig.setDeferredWrite(true);

			// 创建环境
			jeconf.dbEnv = new BdbEnvironment(envFile, envConfig);
			// 打开数据库
			jeconf.db = jeconf.dbEnv.openDatabase(null, dbName, dbConfig);
			
		} catch (DatabaseNotFoundException e) {
			throw e;
		} catch (DatabaseExistsException e) {
			throw e;
		} catch (DatabaseException e) {
			throw e;
		} catch (IllegalArgumentException e) {
			throw e;
		}
		return jeconf;

	}
	
	/* (non-Javadoc)
	 * @see org.apache.flume.channel.AbstractChannel#configure(org.apache.flume.Context)
	 */
	@Override
	  public void configure(Context context) {
		if (channelCounter == null) {
		      channelCounter = new ChannelCounter(getName());
		    }
		Integer batch = null;
	    try {
	    	batch = context.getInteger("transactionCapacity", defaultCapacity);
	    } catch(NumberFormatException e) {
	    	batch = defaultCapacity;
	      LOG.warn("Invalid capacity specified, initializing channel to default capacity of {}", defaultCapacity);
	    }
	    if (batch <= 0) {
	    	batch = defaultCapacity;
	        LOG.warn("Invalid capacity specified, initializing channel to default capacity of {}", defaultCapacity);
	      }
	    transCapacity=batch;
	    
	    String homePath = System.getProperty("user.home").replace('\\', '/');
	    dbDir =context.getString("bdbdir",homePath + "/.flume/je-channel/db");
	    if(jf==null){
	    	jf=createDatabase(this.dbDir,this.dbName);
	    }
	    if(queue != null) {
	    	//queue.clear();
	    }else {
			queue=new JEPersistentQueue<FlumeEvent>(jf.db,FlumeEvent.class,jf.dbEnv.getClassCatalog());
	    }
	}
	
	 /* (non-Javadoc)
 	 * @see org.apache.flume.channel.AbstractChannel#start()
 	 */
 	@Override
	  public synchronized void start() {
	    channelCounter.start();
	    channelCounter.setChannelSize(queue.size());
	    channelCounter.setChannelCapacity(Long.MAX_VALUE);
	    super.start();
	  }

	  /* (non-Javadoc)
  	 * @see org.apache.flume.channel.AbstractChannel#stop()
  	 */
  	@Override
	  public synchronized void stop() {
	    channelCounter.setChannelSize(queue.size());
	    channelCounter.stop();
	    super.stop();
	  }
	
	
	/**
	 * Gets the db dir.
	 *
	 * @return the db dir
	 */
	public String getDbDir() {
		return dbDir;
	}
	
	/**
	 * Gets the channel counter.
	 *
	 * @return the channel counter
	 */
	public ChannelCounter getChannelCounter(){
		return channelCounter;
	}
}
