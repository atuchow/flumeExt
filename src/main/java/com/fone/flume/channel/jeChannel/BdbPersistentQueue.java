/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.channel.jeChannel;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.io.FileUtils;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.collections.StoredMap;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.DatabaseExistsException;
import com.sleepycat.je.DatabaseNotFoundException;
import com.sleepycat.je.EnvironmentConfig;

// TODO: Auto-generated Javadoc
/**
 * 持久化队列,基于BDB实现,也继承Queue,以及可以序列化.但不等同于Queue的时,不再使用后需要关闭
 * 相比一般的内存Queue,插入和获取值需要多消耗一定的时间
 * 这里为什么是继承AbstractQueue而不是实现Queue接口,是因为只要实现offer,peek,poll几个方法即可,
 * 其他如remove,addAll,AbstractQueue会基于这几个方法去实现
 * 
 * @author Phoenics Chow
 * @param <E>
 *            the element type
 */
public class BdbPersistentQueue<E> extends AbstractQueue<E> implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3427799316155220967L;

	/** The db env.数据库环境,无需序列化*/
	private transient BdbEnvironment dbEnv; 

	/** The queue db.  数据库,用于保存值,使得支持队列持久化,无需序列化*/
	private transient Database queueDb; 

	/** The queue map. 持久化Map,Key为指针位置,Value为值,无需序列化*/
	private transient StoredMap<Long, E> queueMap; 

	/** The db dir. */
	private transient String dbDir; // 数据库所在目录

	/**  数据库名字. */
	private transient String dbName; 

	/** The head index.  头部指针*/
	private AtomicLong headIndex; 

	/** The tail index. 尾部指针*/
	private AtomicLong tailIndex;

	/** The peek item. 当前获取的值*/
	private transient E peekItem = null;

	/**
	 * 构造函数,传入BDB数据库.
	 * 
	 * @param db
	 *            the db
	 * @param valueClass
	 *            the value class
	 * @param classCatalog
	 *            the class catalog
	 */
	public BdbPersistentQueue(Database db, Class<E> valueClass, StoredClassCatalog classCatalog) {
		this.queueDb = db;
		this.dbName = db.getDatabaseName();
		headIndex = new AtomicLong(0);
		tailIndex = new AtomicLong(0);
		bindDatabase(queueDb, valueClass, classCatalog);
	}

	/**
	 * 构造函数,传入BDB数据库位置和名字,自己创建数据库.
	 * 
	 * @param dbDir
	 *            the db dir
	 * @param dbName
	 *            the db name
	 * @param valueClass
	 *            the value class
	 */
	public BdbPersistentQueue(String dbDir, String dbName, Class<E> valueClass) {
		headIndex = new AtomicLong(0);
		tailIndex = new AtomicLong(0);
		this.dbDir = dbDir;
		this.dbName = dbName;
		createAndBindDatabase(dbDir, dbName, valueClass);
	}

	/**
	 * 绑定数据库.
	 * 
	 * @param db
	 *            the db
	 * @param valueClass
	 *            the value class
	 * @param classCatalog
	 *            the class catalog
	 */
	public void bindDatabase(Database db, Class<E> valueClass, StoredClassCatalog classCatalog) {
		EntryBinding<E> valueBinding = TupleBinding.getPrimitiveBinding(valueClass);
		if (valueBinding == null) {
			valueBinding = new SerialBinding<E>(classCatalog, valueClass); // 序列化绑定
		}
		queueDb = db;
		queueMap = new StoredSortedMap<Long, E>(db, TupleBinding.getPrimitiveBinding(Long.class), valueBinding, true/* 允许写 */);
		
	}

	/**
	 * 创建以及绑定数据库.
	 * 
	 * @param dbDir
	 *            the db dir
	 * @param dbName
	 *            the db name
	 * @param valueClass
	 *            the value class
	 * @throws DatabaseNotFoundException
	 *             the database not found exception
	 * @throws DatabaseExistsException
	 *             the database exists exception
	 * @throws DatabaseException
	 *             the database exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 */
	private void createAndBindDatabase(String dbDir, String dbName, Class<E> valueClass) throws DatabaseNotFoundException, DatabaseExistsException,
			DatabaseException, IllegalArgumentException {
		File envFile = null;
		EnvironmentConfig envConfig = null;
		DatabaseConfig dbConfig = null;
		Database db = null;

		try {
			// 数据库位置
			envFile = new File(dbDir);

			// 数据库环境配置
			envConfig = new EnvironmentConfig();
			envConfig.setAllowCreate(true);
			envConfig.setTransactional(false);

			// 数据库配置
			dbConfig = new DatabaseConfig();
			dbConfig.setAllowCreate(true);
			dbConfig.setTransactional(false);
			dbConfig.setDeferredWrite(true);

			// 创建环境
			dbEnv = new BdbEnvironment(envFile, envConfig);
			// 打开数据库
			db = dbEnv.openDatabase(null, dbName, dbConfig);
			// 绑定数据库
			bindDatabase(db, valueClass, dbEnv.getClassCatalog());

		} catch (DatabaseNotFoundException e) {
			throw e;
		} catch (DatabaseExistsException e) {
			throw e;
		} catch (DatabaseException e) {
			throw e;
		} catch (IllegalArgumentException e) {
			throw e;
		}

	}

	/**
	 * 值遍历器.
	 * 
	 * @return the iterator
	 */
	@Override
	public Iterator<E> iterator() {
		return queueMap.values().iterator();
	}

	/**
	 * 大小.
	 * 
	 * @return the int
	 */
	@Override
	public int size() {
		synchronized (tailIndex) {
			synchronized (headIndex) {
				return (int) (tailIndex.get() - headIndex.get());
			}
		}
	}

	/**
	 * 插入值.
	 * 
	 * @param e
	 *            the e
	 * @return true, if successful
	 */
	@Override
	public boolean offer(E e) {
		synchronized (tailIndex) {
			queueMap.put(tailIndex.getAndIncrement(), e); // 从尾部插入
		}
		return true;
	}

	/**
	 * 获取值,从头部获取.
	 * 
	 * @return the e
	 */
	@Override
	public E peek() {
		synchronized (headIndex) {
			if (peekItem != null) {
				return peekItem;
			}
			E headItem = null;
			while (headItem == null && headIndex.get() < tailIndex.get()) { // 没有超出范围
				headItem = queueMap.get(headIndex.get());
				if (headItem != null) {
					peekItem = headItem;
					continue;
				}
				headIndex.incrementAndGet(); // 头部指针后移
			}
			return headItem;
		}
	}

	/**
	 * 移出元素,移出头部元素.
	 * 
	 * @return the e
	 */
	@Override
	public E poll() {
		synchronized (headIndex) {
			E headItem = peek();
			if (headItem != null) {
				queueMap.remove(headIndex.getAndIncrement());
				peekItem = null;
				return headItem;
			}
		}
		return null;
	}

	/**
	 * 关闭,也就是关闭所是用的BDB数据库但不关闭数据库环境.
	 */
	public void close() {
		try {
			if (queueDb != null) {
				queueDb.sync();
				queueDb.close();
			}
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 清理,会清空数据库,并且删掉数据库所在目录,慎用.如果想保留数据,请调用close()
	 */
	@Override
	public void clear() {
		try {
			close();
			if (dbEnv != null && queueDb != null) {
				dbEnv.removeDatabase(null, dbName == null ? queueDb.getDatabaseName() : dbName);
				dbEnv.close();
			}
		} catch (DatabaseNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (this.dbDir != null) {
					FileUtils.deleteDirectory(new File(this.dbDir));
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
