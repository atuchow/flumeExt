/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.channel.jeChannel;

import java.io.File;


import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

// TODO: Auto-generated Javadoc
/**
 * 新建一个存储的环境，这里使用JE环境. 
 *
 * @author Phoenics Chow
 */
public class BdbEnvironment extends Environment {
    
    /** The class catalog.  */
    StoredClassCatalog classCatalog; 
    
    /** The class catalog db. */
    Database classCatalogDB;
	
	/**
	 * Instantiates a new bdb environment.
	 *
	 * @param envHome the env home
	 * @param envConfig the env config
	 * @throws DatabaseException the database exception 
	 */
	public BdbEnvironment(File envHome, EnvironmentConfig envConfig) throws DatabaseException {
        super(envHome, envConfig);
    }
	 
 	/**
 	 * Gets the class catalog.
 	 *
 	 * @return the class catalog
 	 */
 	public StoredClassCatalog getClassCatalog() {
	        if(classCatalog == null) {
	            DatabaseConfig dbConfig = new DatabaseConfig();
	            dbConfig.setAllowCreate(true);
	            try {
	                classCatalogDB = openDatabase(null, "classCatalog", dbConfig);
	                classCatalog = new StoredClassCatalog(classCatalogDB);
	            } catch (DatabaseException e) {
	                // TODO Auto-generated catch block
	                throw new RuntimeException(e);
	            }
	        }
	        return classCatalog;
	    }

	    /* (non-Javadoc)
    	 * @see com.sleepycat.je.Environment#close()
    	 */
    	@Override
	    public synchronized void close() throws DatabaseException {
	        if(classCatalogDB!=null) {
	            classCatalogDB.close();
	        }
	        super.close();
	    }
	

}
