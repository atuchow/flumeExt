/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.channel.jeChannel;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;
import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.io.FileUtils;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.collections.TransactionRunner;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.DatabaseNotFoundException;
import com.sleepycat.je.Environment;
// TODO: Auto-generated Javadoc

/**
 * The Class JEPersistentQueue.
 *
 * @author Phoenics Chow
 * @param <E> the element type
 */
@SuppressWarnings("unused")
public class JEPersistentQueue<E> extends AbstractQueue<E> implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9107486660001664830L;
	/** The db env.数据库环境,无需序列化*/
	private transient BdbEnvironment dbEnv; 

	/** The queue db.  数据库,用于保存值,使得支持队列持久化,无需序列化*/
	private transient Database queueDb; 

	/** The queue map. 持久化Map,Key为指针位置,Value为值,无需序列化*/
	private transient StoredSortedMap<Long, E> queueMap; 

	/** The db dir. */
	private transient String dbDir; // 数据库所在目录

	/**  数据库名字. */
	private transient String dbName; 

	/** The head index.  头部指针*/
	private AtomicLong headIndex; 

	/** The tail index. 尾部指针*/
	private AtomicLong tailIndex;

	/** The peek item. 当前获取的值*/
	private transient E peekItem = null;
	
	/** The capacity bound, or Integer.MAX_VALUE if none */
	private final int capacity;
	
	/**  Lock held by take, poll, etc. */
	private final ReentrantLock takeLock = new ReentrantLock();

	/**  Wait queue for waiting takes. */
	private final Condition notEmpty = takeLock.newCondition();

	/**  Lock held by put, offer, etc. */
	private final ReentrantLock putLock = new ReentrantLock();

	/**  Wait queue for waiting puts. */
	private final Condition notFull = putLock.newCondition();
	
	/**
	 * Signals a waiting take. Called only from put/offer (which do not
	 * otherwise ordinarily lock takeLock.)
	 */
	private void signalNotEmpty() {
		final ReentrantLock takeLock = this.takeLock;
		takeLock.lock();
		try {
			notEmpty.signal();
		} finally {
			takeLock.unlock();
		}
	}

	/**
	 * Signals a waiting put. Called only from take/poll.
	 */
	private void signalNotFull() {
		final ReentrantLock putLock = this.putLock;
		putLock.lock();
		try {
			notFull.signal();
		} finally {
			putLock.unlock();
		}
	}
	
	/**
	 * Lock to prevent both puts and takes.
	 */
	void fullyLock() {
		putLock.lock();
		takeLock.lock();
	}

	/**
	 * Unlock to allow both puts and takes.
	 */
	void fullyUnlock() {
		takeLock.unlock();
		putLock.unlock();
	}
	
	/**
	 * Instantiates a new JE persistent queue.
	 *
	 * @param db the db
	 * @param valueClass the value class
	 * @param classCatalog the class catalog
	 */
	public JEPersistentQueue(Database db, Class<E> valueClass, StoredClassCatalog classCatalog) {
		//Class <E>  entityClass  =  (Class <E> ) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this(Integer.MAX_VALUE,db,valueClass,classCatalog);
	}
	
	/**
	 * Instantiates a new JE persistent queue.
	 *
	 * @param capacity the capacity
	 * @param db the db
	 * @param valueClass the value class
	 * @param classCatalog the class catalog
	 */
	public JEPersistentQueue(int capacity,Database db, Class<E> valueClass, StoredClassCatalog classCatalog) {
		if (capacity <= 0)
			throw new IllegalArgumentException();
		this.capacity = capacity;
		this.queueDb = db;
		this.dbName = db.getDatabaseName();
		headIndex = new AtomicLong(0);
		tailIndex = new AtomicLong(0);
		bindDatabase(queueDb, valueClass, classCatalog);
	}
	
	/**
	 * Bind database.
	 *
	 * @param db the db
	 * @param valueClass the value class
	 * @param classCatalog the class catalog
	 */
	private void bindDatabase(Database db, Class<E> valueClass, StoredClassCatalog classCatalog) {
		EntryBinding<E> valueBinding = new SerialBinding<E>(classCatalog, valueClass); // 序列化绑定
		EntryBinding<Long> keyBinding = new SerialBinding<Long>(classCatalog, Long.class); // 序列化绑定
		queueMap = new StoredSortedMap<Long, E>(db, keyBinding, valueBinding, true/* 允许写 */);
		init(queueMap);
	}
	
	/**
	 * Inits the.
	 *
	 * @param queueMap the queue map
	 */
	private void init(StoredSortedMap<Long, E> queueMap){
		fullyLock();
		try {
			if(this.size()==0){
				return;
			}
			headIndex.set(queueMap.firstKey());
			tailIndex.set(queueMap.lastKey()+1);
		} finally {
			fullyUnlock();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Queue#offer(java.lang.Object)
	 */
	@Override
	public boolean offer(E e) {
		if (e == null)
			throw new NullPointerException();
		if (this.size() == capacity)
			return false;
		final ReentrantLock putLock = this.putLock;
		int c=-1;
		putLock.lock();
		try {
			c=this.size();
			if (c < capacity) {
				queueMap.put(tailIndex.getAndIncrement(), e); // 从尾部插入
			}
		} finally {
			putLock.unlock();
		}
		return c >= 0;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Queue#poll()
	 */
	@Override
	public E poll() {
		if(this.size()==0){
			return null;
		}
		final ReentrantLock takeLock = this.takeLock;
		takeLock.lock();
		try{
			long l=queueMap.firstKey();
			E headItem =queueMap.remove(l);
			if (headItem != null) {
				peekItem = null;
				return headItem;
			}
		}finally {
			takeLock.unlock();
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Queue#peek()
	 */
	@Override
	public E peek() {
		if(this.size()==0){
			return null;
		}
		final ReentrantLock takeLock = this.takeLock;
		takeLock.lock();
		try {
			if (peekItem != null) {
				return peekItem;
			}
			E headItem = null;
			long l=queueMap.firstKey();
			if(headIndex.get()<l){
				headIndex.set(l);
			}
			while (headItem == null && headIndex.get() < tailIndex.get()) { // 没有超出范围
				headItem = queueMap.get(headIndex.get());
				if (headItem != null) {
					peekItem = headItem;
					continue;
				}
				headIndex.incrementAndGet(); // 头部指针后移
			}
			return headItem;
		} finally {
			takeLock.unlock();
		}
	}
	
	/**
	 * Peek.
	 *
	 * @param p the p
	 * @return the e
	 */
	private E peek(long p){
		if(this.size()==0){
			return null;
		}
		final ReentrantLock takeLock = this.takeLock;
		takeLock.lock();
		try {
			long l=queueMap.firstKey();
			return queueMap.get(queueMap.firstKey());
		} finally {
			takeLock.unlock();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return queueMap.values().iterator();
	}
	
	
	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#size()
	 */
	@Override
	public int size() {
		return queueMap.size();
	}
	/**
	 * 关闭,也就是关闭所是用的BDB数据库但不关闭数据库环境.
	 */
	public void close() {
		try {
			if (queueDb != null) {
				queueDb.sync();
				queueDb.close();
			}
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 清理,会清空数据库,并且删掉数据库所在目录,慎用.如果想保留数据,请调用close()
	 */
	@Override
	public void clear() {
		try {
			close();
			if (dbEnv != null && queueDb != null) {
				dbEnv.removeDatabase(null, dbName == null ? queueDb.getDatabaseName() : dbName);
				dbEnv.close();
			}
		} catch (DatabaseNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (this.dbDir != null) {
					FileUtils.deleteDirectory(new File(this.dbDir));
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
