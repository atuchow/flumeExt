/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fone.flume.channel.jeChannel;

import java.io.Serializable;
import java.util.Map;

import org.apache.flume.Event;

// TODO: Auto-generated Javadoc
/**
 * Persistable wrapper for Event.
 *
 * @author Phoenics Chow
 */
class FlumeEvent implements Event,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3699486009474580087L;

/** The headers. */
private Map<String, String> headers;
  
  /** The body. */
  private byte[] body;

  /**
   * Instantiates a new flume event.
   *
   * @param headers the headers
   * @param body the body
   */
  FlumeEvent(Map<String, String> headers, byte[] body) {
    this.headers = headers;
    this.body = body;
  }

  /* (non-Javadoc)
   * @see org.apache.flume.Event#getHeaders()
   */
  @Override
  public Map<String, String> getHeaders() {
    return headers;
  }

  /* (non-Javadoc)
   * @see org.apache.flume.Event#setHeaders(java.util.Map)
   */
  @Override
  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
  }

  /* (non-Javadoc)
   * @see org.apache.flume.Event#getBody()
   */
  @Override
  public byte[] getBody() {
    return body;
  }

  /* (non-Javadoc)
   * @see org.apache.flume.Event#setBody(byte[])
   */
  @Override
  public void setBody(byte[] body) {
    this.body = body;
  }

}
